# tilt

Simple version of a sample in a scattering plane controlled by 3 motors as per the main scatterometer project.  This application was developed primarily for mathematical reasoning, but may be merged into the main application.

## Development Updates:
- https://youtu.be/qpx6Ld4Co4Y