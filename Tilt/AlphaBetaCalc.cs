using Godot;
using System;

public class home : Node
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
    [Signal]
    public delegate void HomeSignal();

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        EmitSignal(nameof(HomeSignal));
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_SpinBoxBeta_value_changed(float value){
        GD.Print("Beta: "+value.ToString());
    }
    private void _on_SpinBoxAlpha_value_changed(float value){
        GD.Print("Alpha: "+value.ToString());
    }
}
