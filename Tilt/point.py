"""Module for points in space."""
from math import sqrt
from fself import fself
class Point2:
    """A class for points in 2 dimensional space."""
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.dimension_count = 2

class Point3:
    """A class for points in 3 dimensional space."""
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.dimension_count = 3
    def __str__(self):
        return f'({self.x},{self.y},{self.z})'

class Point:
    """A class for a point in n dimensional space."""
    def __init__(self, *coordinates):
        self.coordinates = coordinates
        self.dimension_count = len(coordinates)

def distance_from(α, β):
    """Measures distance between two points"""
    if α.dimension_count == β.dimension_count:
        if isinstance(α, Point2):
            α = Point((α.x, α.y))
        if isinstance(β, Point2):
            β = Point((β.x, β.y))
        if isinstance(α, Point3):
            α = Point((α.x, α.y, α.z))
        if isinstance(β, Point3):
            β = Point((β.x, β.y, β.z))
        return sqrt(
            sum(
                [pow((_[0]-_[1]), 2) for _ in zip(α.coordinates, β.coordinates)]
            )
        )
    else:
        raise NotImplementedError(fself().__doc__)

def distance_test():
    """Simple function for visually checking results of distance function."""
    first_list = []
    second_list = []
    upper_bound = 4
    for counter in range(upper_bound):
        first_list.append(0)
        second_list.append(1)

        point_a = Point(*first_list)
        point_b = Point(*second_list)

        print(counter+1, distance_from(point_a, point_b))
if __name__ == '__main__':
    distance_test()
