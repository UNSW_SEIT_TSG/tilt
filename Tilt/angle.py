from __future__ import annotations
from math import tau as τ
from math import radians, degrees
from human_error import HumanError
import random


class Angle():
    def __init__(self,
                 deg: float=None,
                 rad: float=None,
                 turn: float=None,
                 rand: tuple=None):
        a = [deg, rad, turn, rand]
        not_none_count = sum([1 for _ in a if _ is not None])

        if not_none_count == 0:
            self._degrees = None
            self._radians = None
            self._turns = None
        elif not_none_count == 1:
            if deg is not None:
                # set deg
                self.setDegrees(deg)
            elif rad is not None:
                # set rad
                self.setRadians(rad)
            elif turn is not None:
                # set turn
                self.setTurns(turn)
            elif rand is not None:
                # set turn
                self.uniform(rand[0], rand[1])
            else:
                raise HumanError("Developer assumption incorrect")
        else:
            raise ValueError("Maximum of one value may be specified")
    
    def getDegrees(self):
        return self._degrees

    def setDegrees(self, value):
        self._degrees = value
        self._radians = radians(value)
        self._turns = value/360

    def getRadians(self):
        return self._radians
    
    def setRadians(self, value):
        self._radians = value
        self._degrees = degrees(value)
        self._turns = value/τ
    
    def getTurns(self):
        return self._turns

    def setTurns(self, value):
        self._turns = value
        self._radians = radians(value*τ)
        self._degrees = value*360
    
    def uniform(self, lower, upper):
        self.setTurns(random.random()*(upper.turns - lower.turns)+lower.turns)

    def __add__(self: Angle, other: Angle):
        return Angle(turn=self.turns + other.turns)

    def __sub__(self: Angle, other: Angle):
        return Angle(turn=self.turns - other.turns)

    def __neg__(self: Angle):
        return Angle(turn=self.turns*-1)

    degrees = property(getDegrees, setDegrees)
    radians = property(getRadians, setRadians)
    turns = property(getTurns, setTurns)