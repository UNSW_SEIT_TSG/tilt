"""This module contains spatial transformation functions."""
import numpy as np
from vector import Vector3


def rotate_vector_about_axis(vec: np.array, rot_axis: np.array, deg: float):
    """rotate 3d vector around a unit vector axis"""
    if isinstance(vec, Vector3):
        vec = np.array([vec.a, vec.b, vec.c])
    if isinstance(rot_axis, Vector3):
        rot_axis = np.array([rot_axis.a, rot_axis.b, rot_axis.c])
    
    ux, uy, uz = rot_axis
    radians = np.radians(deg)
    adj = np.cos(radians)
    opp = np.sin(radians)
    rotation_matrix = np.array([
        [adj+ux*ux*(1-adj), ux*uy*(1-adj)-uz*opp, ux*uz*(1-adj)+uy*opp],
        [uy*ux*(1-adj)+uz*opp, adj+uy*uy*(1-adj), uy*uz*(1-adj)-ux*opp],
        [uz*ux*(1-adj)-uy*opp, uz*uy*(1-adj)+ux*opp, adj+uz*uz*(1-adj)]
    ])
    
    r_x, r_y, r_z = np.dot(rotation_matrix, vec)
    return Vector3(x=r_x, y=r_y, z=r_z, magnitude=1)
    