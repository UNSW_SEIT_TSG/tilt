from angle import Angle


class stack():
    def __init__(self):
        self.θ_7 = Angle(deg=0)
        self.θ_7_min = Angle(deg=-90)
        self.θ_7_max = Angle(deg=90)

        self.θ_5 = Angle(deg=90)
        self.θ_5_min = Angle(deg=0)
        self.θ_5_max = Angle(deg=180)

    def θ_7Fromα(self, α:Angle):
        self.θ_7 = Angle(turn=α.turns)

    def θ_5FromαAndβ(self, α:Angle, β:Angle):
        self.θ_5 = Angle()

def main(α, β):
    print("/"+"="*78+"\\")
    print(f'α: {α.degrees} [degrees]')
    print(f'β: {β.degrees} [degrees]')
    z = stack()
    
    print("\\"+"="*78+"/")

if __name__ == "__main__":
    α_min = Angle(deg=-90)
    α_max = Angle(deg=90)
    α = Angle(rand=(α_min,α_max))
        
    β_min = Angle(deg=0)
    β_max = Angle(deg=180)
    β = Angle(rand=(β_min,β_max))

    main(α, β)
