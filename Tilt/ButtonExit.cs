using Godot;
using System;

public class ButtonExit : Button
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
    private void _on_ButtonExit_pressed(){
        GD.Print("_on_ButtonExit_pressed()");
        GetTree().Quit();
    }
    private void _on_Timer_timeout(){
        GD.Print("_on_Timer_timeout");
        this.SetText("Boo");
    }
}
