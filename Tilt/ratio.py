from root import Root
class Ratio:
    """Class for ratios"""
    def __init__(self, numerator, denominator):
        """Creates ratio object"""
        self.numerator = numerator
        self.denominator = denominator
        self.simplify()
    def simplify(self):
        largest_common_factor = None
        while largest_common_factor != 1:
            largest_common_factor = self.simplify_step()
    def get_largest_common_factor(self, left_number:int, right_number:int):
        largest_common_factor = 1
        smaller_number = min(left_number, right_number)
        for number in range(smaller_number, 1, -1):
            left_num_divided_by_number = left_number/number
            right_number_divided_by_number = right_number/number
            if (
                left_num_divided_by_number == int(left_num_divided_by_number) and
                right_number_divided_by_number == int(right_number_divided_by_number)
            ):
                largest_common_factor = number
                break
        return largest_common_factor
    def simplify_step(self):
        """Simplifies ratio"""
        largest_common_factor = 1
        left_number = 1
        right_number = 1

        if isinstance(self.numerator, Root):
            left_number = abs(self.numerator.external)
        elif isinstance(self.numerator, int):
            left_number = abs(self.numerator)
        else:
            raise NotImplementedError

        if isinstance(self.denominator, Root):
            right_number = abs(self.denominator.external)
        elif isinstance(self.denominator, int):
            right_number = abs(self.denominator)
        else:
            raise NotImplementedError

        largest_common_factor = self.get_largest_common_factor(left_number, right_number)

        if isinstance(self.numerator, Root):
            self.numerator = Root(
                external=int(self.numerator.external/largest_common_factor),
                internal=self.numerator.internal
            )
        elif isinstance(self.numerator, int):
            self.numerator = int(self.numerator/largest_common_factor)
        else:
            raise NotImplementedError

        if isinstance(self.denominator, Root):
            self.denominator = Root(
                external=int(self.denominator.external/largest_common_factor),
                internal=self.denominator.internal
            )
        elif isinstance(self.denominator, int):
            self.denominator = int(self.denominator/largest_common_factor)
        else:
            raise NotImplementedError

        return largest_common_factor
    def __str__(self):
        if self.denominator == 1:
            return f'{self.numerator}'
        else:
            return f'{self.numerator}/{self.denominator}'
    def square(self):
        return Ratio(
            numerator=self.numerator*self.numerator,
            denominator=self.denominator*self.denominator
        )
    def __add__(self, other):
        return Ratio(
            numerator = self.numerator*other.denominator + self.denominator*other.numerator,
            denominator = self.denominator*other.denominator
        )
    def __mul__(self, other):
        if isinstance(other, Ratio):
            ratio = Ratio(
                numerator=self.numerator*other.numerator, 
                denominator=self.denominator*other.denominator
            )
            return ratio.numerator if ratio.denominator == 1 else ratio
        elif isinstance(other, int):
            ratio = Ratio(
                numerator=self.numerator*other, 
                denominator=self.denominator
            )
            return ratio.numerator if ratio.denominator == 1 else ratio
        else:
            raise NotImplementedError