"""Module for 2 and 3 dimensinoal vector classes"""
# Standard library imports
from math import acos, sin, cos, atan2, degrees, radians, sqrt, pi
import numpy as np

# Custom library imports
from root import Root
from ratio import Ratio
from point import Point3
# from plane import Plane

from fself import fself


class Vector2:
    """Class for 2 dimensional vectors"""
    def __init__(self,
                 x=None,
                 y=None,
                 magnitude=None,
                 heading_rad=None,
                 heading_degrees=None):
        """Vector2 Object initialisation function."""
        if x is not None and y is not None:
            # Vector provided in x y component form
            self.x = x
            self.y = y
            self.magnitude = self.get_magnitude()
            self.heading_rad = self.get_heading()
            self.heading_degrees = degrees(self.get_heading())
        elif (magnitude is not None and (
                (heading_rad is not None and heading_degrees is None) or
                (heading_rad is None and heading_degrees is not None))):
            # Vector provided in magnitude heading component form
            self.magnitude = magnitude
            if heading_rad is not None:
                self.heading_rad = heading_rad
                self.heading_degrees = degrees(self.heading_rad)
            else:
                self.heading_degrees = heading_degrees
                self.heading_rad = radians(self.heading_degrees)
            self.x = magnitude * cos(self.heading_rad)
            self.y = magnitude * sin(self.heading_rad)
        else:
            # This case should not occur, but treated as exception for now.
            raise NotImplementedError

    def get_heading(self):
        """Returns heading of vector."""
        return atan2(self.y, self.x)

    def get_magnitude(self):
        """Returns magnitude of vector"""
        return Root(
            external=1,
            internal=(self.x*self.x+self.y*self.y)
        )

    def get_unit_vector(self):
        """Returns a new unit vector from initial vector."""
        magnitude = self.get_magnitude()
        ratio_x = Ratio(numerator=self.x, denominator=magnitude)
        return Vector2(
            x=ratio_x,
            y=Ratio(numerator=self.y, denominator=magnitude)
        )

    def __str__(self):
        """Converts Vector2 object to a string"""
        if isinstance(self.x, float) and isinstance(self.x, float):
            self.magnitude = self.get_magnitude()
            self.heading_degrees = degrees(self.get_heading())
            if self.heading_degrees < 0:
                self.heading_degrees = 360 + self.heading_degrees
            return ''.join([
                f'({self.x:0.3f}, {self.y:0.3f})',
                ' or ',
                f'({self.magnitude}, {self.heading_degrees:0.3f}°)'
            ])
        elif isinstance(self.x, Ratio) and isinstance(self.x, Ratio):
            return f'({self.x},{self.y})'
        elif isinstance(self.x, int) and isinstance(self.x, int):
            return f'({self.x: >3},{self.y: >3})'
        else:
            raise NotImplementedError

    def __add__(self, other):
        """Performs 2 dimensional vector addition"""
        return Vector2(x=self.x+other.x, y=self.y+other.y)

    def __sub__(self, other):
        """Performs 2 dimensional vector subtraction"""
        return Vector2(x=self.x-other.x, y=self.y-other.y)

    def rotated(self, dθ_degrees: float):
        """Returns new vector rotated by some angle in degrees."""
        return Vector2(
            magnitude=self.magnitude,
            heading_degrees=self.heading_degrees+dθ_degrees
        )


class Vector3:
    """Class for 3 dimensional vectors"""
    def __init__(self,
                 x=None,
                 y=None,
                 z=None,
                 magnitude=None,
                 α_rad=None,
                 β_rad=None,
                 γ_rad=None,
                 α_deg=None,
                 β_deg=None,
                 γ_deg=None,
                 is_unit_vector=False):
        """
        Initialisation function for new vector3 objects.
        Inputs:
            x
            y
            z
            magnitude
            α is the angle between the vector and the x-axis
                α_rad
                α_deg
            β is the angle between the vector and the y-axis
                β_rad
                β_deg
            γ is the angle between the vector and the z-axis
                γ_rad
                γ_deg
        """
        if not is_unit_vector:
            self.u_x = Vector3(
                α_rad=0,
                β_rad=pi/2,
                γ_rad=pi/2,
                magnitude=1,
                is_unit_vector=True
            )
            self.u_y = Vector3(
                α_rad=pi/2,
                β_rad=0,
                γ_rad=pi/2,
                magnitude=1,
                is_unit_vector=True
            )
            self.u_z = Vector3(
                α_rad=pi/2,
                β_rad=pi/2,
                γ_rad=0,
                magnitude=1,
                is_unit_vector=True
            )
        if x is not None and y is not None and z is not None:
            self.x = x
            self.y = y
            self.z = z
            if x == 0 and y == 0 and z == 0:
                self.magnitude = 0
                self.is_zero_vector = True
            else:
                self.is_zero_vector = False
                self.compute_magnitude()
        elif magnitude is not None:
            self.magnitude = magnitude
            α_available = α_rad is not None or α_deg is not None
            β_available = β_rad is not None or β_deg is not None
            γ_available = γ_rad is not None or γ_deg is not None
            if α_available and β_available and γ_available:
                self.α_rad = α_rad if α_rad is not None else radians(α_deg)
                self.β_rad = β_rad if β_rad is not None else radians(β_deg)
                self.γ_rad = γ_rad if γ_rad is not None else radians(γ_deg)
                self.compute_x_y_z()
            else:
                raise NotImplementedError(
                    'Vector3 creation failed due to invalid α β γ values'
                )
        else:
            raise NotImplementedError

        if α_rad is None:
            self.compute_α()
        if β_rad is None:
            self.compute_β()
        if γ_rad is None:
            self.compute_γ()

    @property
    def a(self):
        return self.x

    @property
    def b(self):
        return self.y

    @property
    def c(self):
        return self.z

    def __add__(self, other):
        """Performs 2 dimensional vector addition"""
        return Vector3(x=self.x+other.x, y=self.y+other.y, z=self.z+other.z)

    def __sub__(self, other):
        """Performs 2 dimensional vector subtraction"""
        return Vector3(x=self.x-other.x, y=self.y-other.y, z=self.z-other.z)

    def __mul__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            return self.scaled(other)
        else:
            raise NotImplementedError(fself().__doc__)

    def __str__(self):
        return '\n'.join([
            f' <x: {self.x:.2f}, y: {self.y:.2f}, z: {self.z:.2f}>',
            # f' magnitude: {self.magnitude}',
            # f' α_rad: {self.α_rad/pi:.2f}π [rad] = {degrees(self.α_rad):.2f}°',
            # f' β_rad: {self.β_rad/pi:.2f}π [rad] = {degrees(self.β_rad):.2f}°',
            # f' γ_rad: {self.γ_rad/pi:.2f}π [rad] = {degrees(self.γ_rad):.2f}°',
            # ''
        ])

    def normalised(self):
        """Returns a copy of the original vector scaled to be of length 1."""
        self.compute_magnitude()
        return self.scaled(1/self.magnitude)

    def scaled(self, other):
        """Returns a copy of the original vector, scaled by some scalar."""
        if isinstance(other, float) or isinstance(other, int):
            return Vector3(
                x=self.x*other,
                y=self.y*other,
                z=self.z*other
            )
        else:
            raise NotImplementedError(fself().__doc__)

    def compute_α(self):
        """Compute α from x, y, z"""
        if not self.is_zero_vector:
            self.α_rad = self.compute_angle_between_vectors(self.u_x)
        else:
            self.α_rad = None

    def compute_β(self):
        """Compute β from x, y, z"""
        if not self.is_zero_vector:
            self.β_rad = self.compute_angle_between_vectors(self.u_y)
        else:
            self.β_rad = None

    def compute_γ(self):
        """Compute γ from x, y, z"""
        if not self.is_zero_vector:
            self.γ_rad = self.compute_angle_between_vectors(self.u_z)
        else:
            self.γ_rad = None

    def compute_x_y_z(self):
        """Compute x, y, z from α, β, γ"""
        self.x = cos(self.α_rad)*self.magnitude
        self.y = cos(self.β_rad)*self.magnitude
        self.z = cos(self.γ_rad)*self.magnitude

    def compute_magnitude(self):
        """Compute magnitude from x, y, z"""
        self.magnitude = sqrt(pow(self.x, 2)+pow(self.y, 2)+pow(self.z, 2))

    def compute_angle_between_vectors(self, other):
        """Returns angle between self and other measured in radians."""
        self_other_dot_product = self.compute_dot_product_between_vectors(other)
        self_magnitude_by_other_magnitude = self.magnitude*other.magnitude
        ratio = self_other_dot_product/self_magnitude_by_other_magnitude
        return acos(ratio)

    def compute_dot_product_between_vectors(self, other):
        """Returns dot product of vectors: self and other."""
        return self.x*other.x + self.y*other.y + self.z*other.z

    def set_magnitude(self, magnitude: float):
        """Sets magnitude property of vector, and updates x, y, and z values."""
        temp_vec = Vector3(
            magnitude=magnitude,
            α_rad=self.α_rad,
            β_rad=self.β_rad,
            γ_rad=self.γ_rad
        )
        self.magnitude = temp_vec.magnitude
        self.compute_x_y_z()

    def is_orthogonal(self, other):
        """Returns true if vector3 'self' and vector3 'other' are orthogonal."""
        return self.compute_dot_product_between_vectors(other) == 0

    def project_onto_plane(self, plane):
        """Returns vector of self projected onto plane, 'plane'."""
        raise NotImplementedError(fself().__doc__)

    def rotated(
            self,
            axis_of_rotation,
            dθ_degrees: float = None,
            dθ_radians: float = None):
        """Returns new vector rotated by some angle"""
        original_magnitude = self.magnitude
        vec = np.array([self.x, self.y, self.z])
        rot_axis = np.array([axis_of_rotation.x, axis_of_rotation.y, axis_of_rotation.z])

        if dθ_degrees is None and dθ_radians is None:
            raise ValueError('Need to supply angle in either degrees or radians')
        elif dθ_degrees is not None and dθ_radians is None:
            θ = dθ_degrees
        elif dθ_degrees is None and dθ_radians is not None:
            θ = degrees(dθ_radians)
        else:
            raise NotImplementedError(fself().__doc__)

        u_x, u_y, u_z = rot_axis
        adj = np.cos(np.radians(θ))
        opp = np.sin(np.radians(θ))
        rotation_matrix = np.array([
            [adj+u_x*u_x*(1-adj), u_x*u_y*(1-adj)-u_z*opp, u_x*u_z*(1-adj)+u_y*opp],
            [u_y*u_x*(1-adj)+u_z*opp, adj+u_y*u_y*(1-adj), u_y*u_z*(1-adj)-u_x*opp],
            [u_z*u_x*(1-adj)-u_y*opp, u_z*u_y*(1-adj)+u_x*opp, adj+u_z*u_z*(1-adj)]
        ])
        result = np.dot(rotation_matrix, vec)
        x, y, z = result

        result_vec = Vector3(x=x, y=y, z=z)
        result_vec.set_magnitude(original_magnitude)
        print(' '.join([
            f'{θ}:',
            f'<{result_vec.x:.3f},'
            f'{result_vec.y:.3f},'
            f'{result_vec.z:.3f},'
            f'{result_vec.magnitude}>'
        ]))
        return result_vec

def get_vector3_from_start_and_end_point3s(start: Point3, end: Point3):
    """Return a vector that connects start and end points in 3 dimensions."""
    origin_to_start_point_vector = Vector3(x=start.x, y=start.y, z=start.z)
    origin_to_end_point_vector = Vector3(x=end.x, y=end.y, z=end.z)
    return origin_to_end_point_vector - origin_to_start_point_vector

def test_vector_3():
    """test methods of the vector3 class"""
    vec3 = Vector3(x=0, y=2, z=2)
    print(vec3)

    vec_a = Vector3(x=1, y=0, z=0)
    axis = Vector3(x=1, y=1, z=1)
    step = 120
    for θ in range(0, 360+step, step):
        vec_c = vec_a.rotated(axis_of_rotation=axis, dθ_degrees=θ)
        print(vec_c)

def main():
    """Main line of program."""
    raise NotImplementedError(fself().__doc__)

if __name__ == '__main__':
    # main()
    test_vector_3()
