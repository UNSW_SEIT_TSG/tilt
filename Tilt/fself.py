import inspect

def foo():
    """zxy"""
    print(fself().__doc__)

def fself():
    """returns parent function when called"""
    return eval(inspect.stack()[1][3])

def bar():
    """abcdefg"""
    print(fself().__doc__)

def main():
    bar()
    foo()

if __name__ == '__main__':
    main()

