"""Module to model the stack of interconnected stages in the Scatterometer."""
import numpy as np
from copy import deepcopy
from stage import Stage
from spatial import rotate_vector_about_axis
from vector import Vector3
from angle import Angle


class Stack:
    """Models the stack of interconnected stages of the scatterometer."""
    def __init__(self):
        self.sequence = [
            'group_2',
            'group_5',
            'group_7',
            'sample'
        ]
        self.datum = {
            'group_2': Stage(
                axis=Vector3(0.0, -1.0, 0.0),
                θ=Angle(deg=0),
                child='group_5'
            ),
            'group_5': Stage(
                axis=Vector3(-1.0, 0.0, 0.0),
                θ=Angle(deg=0),
                child='group_7',
                parent='group_2'
            ),
            'group_7': Stage(
                axis=Vector3(0.0, -1.0, 0.0),
                θ=Angle(deg=0),
                parent='group_5',
                child='sample',
            ),
            'sample': Stage(
                axis=Vector3(0.0, 0.0, -1.0),
                θ=Angle(deg=0),
                parent='group_75'
            )
        }
        self.stages = deepcopy(self.datum)

    def propagate_motion(self):
        """Apply motion to all child stages"""
        for i in range(len(self.sequence)-1):
            obj = self.stages[self.sequence[i]]
            for j in range(i+1, len(self.sequence)):
                self.stages[self.sequence[j]].axis = rotate_vector_about_axis(
                    vec=self.datum[self.sequence[j]].axis,
                    rot_axis=obj.axis,
                    deg=obj.θ.degrees
                )
                # print(self.sequence[i], self.sequence[j])

    def group_move_absolute(self, group, θ):
        """Move a specific stage to a specific θ."""
        raise NotImplementedError

    def group_move_relative(self, group, dθ: Angle):
        """Move a specific stage by a certain dθ."""
        self.stages[group].θ += dθ

        self.propagate_motion()

        for g in self.stages:
            print(
                f'{g}.axis:   {self.stages[g].axis}, \t'
                f'{g}.θ: {self.stages[g].θ.degrees:0.3f} [degrees]'
            )
        print()
