class HumanError(Exception):
    """Custom exception; flags part of code that should never be reached."""
    pass