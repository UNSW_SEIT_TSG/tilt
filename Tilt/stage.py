"""Module to model an individual motor/stage of the scatterometer."""

from angle import Angle


class Stage:
    """Class for a stage/motor in the scatterometer."""
    def __init__(
            self,
            axis,
            θ: Angle,
            home=0,
            child: str=None,
            parent: str=None):
        """Initialisation function"""
        self.axis = axis
        self.θ = θ
        self.home = home
        self.child = child
        self.parent = parent
    
    def __str__(self):
        return '\n'.join([
            f'  axis: {self.axis}',
            f'  home: {self.home}',
            f'  child: {self.child}',
            f'  θ: {self.θ}',
            f'  parent: {self.parent}'
        ])
        
