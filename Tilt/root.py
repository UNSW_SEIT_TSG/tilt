from math import sqrt, pow
class Root:
    """Class for roots"""
    def __init__(self, external, internal):
        self.external = external
        self.internal = internal
        if isinstance(self.external, float) or isinstance(self.internal, float):
            self.approximate_value = self.approximate()
        else:
            self.simplify()
    def __str__(self):
        if isinstance(self.internal, float):
            self.approximate_value = self.approximate()
            return f'{self.approximate_value:0.3f}'
        else:
            if self.external == 1 and self.internal != 1:
                return f'√({self.internal})'
            elif self.external == 1 and self.internal == 1:
                return f'1'
            elif self.external != 1 and self.internal == 1:
                return f'{self.external}'
            elif self.external != 1 and self.internal != 1:
                return f'{self.external}√({self.internal})'
            else:
                raise NotImplementedError
    def is_square(self, number):
        return pow(int(sqrt(number)),2) == number
    def get_largest_square_factor(self):
        for number in range(self.internal,1,-1):
            if self.internal / number == int(self.internal/number):
                # print(f'{number} is factor of {internal}')
                if self.is_square(number):
                    # print(f'{number} is also a square')
                    return number
        return 1
    def simplify_step(self):
        largest_square_factor = self.get_largest_square_factor()
        self.external = int(self.external*sqrt(largest_square_factor))
        self.internal = int(self.internal/largest_square_factor)
    def simplify(self):    
        last_internal = -1
        while last_internal != self.internal:
            # print(f'last_internal: {last_internal}, self.internal: {self.internal}')
            last_internal = self.internal
            self.simplify_step()
    def approximate(self):
        return self.external * sqrt(self.internal)