"""Module to map motor positions to scatterplane angles and vice versa."""
import pygame
from stack import Stack
from angle import Angle


def process_input(key_pressed, over: bool, dθ: Angle):
    """Function to process keyboard input."""
    group_2_dθ = Angle(deg=0)
    group_5_dθ = Angle(deg=0)
    group_7_dθ = Angle(deg=0)

    over = False
    if key_pressed[pygame.K_ESCAPE]:
        over = True
    elif key_pressed[pygame.K_LEFT]:
        group_2_dθ = dθ
    elif key_pressed[pygame.K_RIGHT]:
        group_2_dθ = -dθ
    elif key_pressed[pygame.K_UP]:
        group_5_dθ = dθ
    elif key_pressed[pygame.K_DOWN]:
        group_5_dθ = -dθ
    elif key_pressed[pygame.K_PAGEUP]:
        group_7_dθ = dθ
    elif key_pressed[pygame.K_PAGEDOWN]:
        group_7_dθ = -dθ
    dθs = (group_2_dθ, group_5_dθ, group_7_dθ)
    return dθs, over


def main():
    """Main function"""
    pygame.init()
    window_surface = pygame.display.set_mode((1000, 1000), 0, 32)
    window_surface.fill((0, 0, 0))

    over = False
    stack = Stack()

    while not over:
        pygame.event.wait()
        (g_2_step, g_5_step, g_7_step), over = process_input(
            pygame.key.get_pressed(),
            over,
            Angle(15)
        )

        if g_2_step.degrees != 0:
            stack.group_move_relative(group='group_2', dθ=g_2_step)
        if g_5_step.degrees != 0:
            stack.group_move_relative(group='group_5', dθ=g_5_step)
        if g_7_step.degrees != 0:
            stack.group_move_relative(group='group_7', dθ=g_7_step)

    print('bye')

if __name__ == '__main__':
    main()
    # test()
